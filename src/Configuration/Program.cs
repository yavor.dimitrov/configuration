﻿using Microsoft.Extensions.Configuration;
using System;

namespace Configuration
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfigurationRoot configuration = InitConfig();
            Settings settings = new();
            configuration.GetSection("Settings").Bind(settings);

            Console.WriteLine($"Bool Setting is: {settings.BoolSetting}");
            Console.WriteLine($"Config Variable is: {settings.ConfigVariable}");
            Console.WriteLine($"SubsectionString is: {settings.SubSection.SubSectionString}");
        }


        public static IConfigurationRoot InitConfig()
        {
            var env = Environment.GetEnvironmentVariable("DOTNETCORE_ENVIRONMENT");

            // Dont forget to set Build Action and Coppy To Output Directory
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env}.json", true, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }


    }
}
