﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuration
{
    public class Settings
    {
        public bool BoolSetting { get; set; }

        public SubSectionSettings SubSection { get; set; }

        public string ConfigVariable { get; set; }

    }
}
